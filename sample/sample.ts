import { Storage } from '../src/storage';
import { readFileSync } from 'fs';
import { join as pathJoin } from 'path';

const fsConfigFilePath = pathJoin(__dirname, 'filesystem.json');
const fsJson = readFileSync(fsConfigFilePath, 'utf-8');
const fsConfig = JSON.parse(fsJson);

const storage = new Storage(fsConfig);
const defaultDisk = storage.default();

async function start() {
  await defaultDisk.put('my-file.txt', 'my file contents');
  const myFileContents = await defaultDisk.get('my-file.txt', 'utf-8');
  console.log(myFileContents);
}

start();

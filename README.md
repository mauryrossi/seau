# @seau/storage

This module is a work in progress.

This module takes inspiration from Laravel's storage facade:

[https://laravel.com/docs/5.8/filesystem](https://laravel.com/docs/5.8/filesystem)

## Things still pending

- GCS implementation without the use of Interoperability API.
- Additional testing & documentation.
- Caching driver.
- Support for multi-part uploads and attachment downloads.
- Support for SFTP and Rackspace.

#### Your sample storage configuration

```typescript
export const storage = new Storage({
  local: {
    driver: 'local',
    root: '/mnt/disks/vol1/assets',
    url: 'https://yoursite/assets'
  },,
  gcs: {
    driver: 'gcs',
    bucket: 'my-main-bucket',
    region: 'us-central1',
    // Not necessary if AWS_ACCESS_KEY_ID && AWS_SECRET_ACCESS_KEY are set
    credentials: {
      accessKeyId: 'your-key-id',
      secretAccessKey: 'your-secret-key'
    }
  },
  default: {
    driver: 's3',
    bucket: 'my-main-bucket',
    region: 'us-east-2',
    // Not necessary if AWS_ACCESS_KEY_ID && AWS_SECRET_ACCESS_KEY are set
    credentials: {
      accessKeyId: 'your-key-id',
      secretAccessKey: 'your-secret-key'
    }
  }
});
```

#### Writing to a file
This method will also create subdirectories if needed.

```typescript
await storage.defaultDisk().put('your-file.txt', 'contents');
```

#### Reading a file

```typescript
const contents = await storage.defaultDisk().get('your-file.txt');
```

#### Moving a file

```typescript
await storage.defaultDisk().move('your-file.txt', 'your-new-file-name.txt');
```

#### Deleting a file

```typescript
await storage.defaultDisk().delete('your-file.txt');
```

#### Determining file size

```typescript
const size = await storage.defaultDisk().size('your-file.txt');
```

#### Generating a signed URL

```typescript
const expireInSeconds = 900;
const url = await storage
  .defaultDisk()
  .temporaryUrl('your-file.txt', expireInSeconds);
```

#### Setting file visibility

```typescript
await storage
  .defaultDisk()
  .setVisibility('your-file.txt', FileVisibility.PUBLIC);
```

#### Determining file visibility

```typescript
const visibility = await storage.defaultDisk().getVisibility('your-file.txt');
```


#### Notes
In order to use Google Cloud Storage, you must enable the Interoperability API here:

[https://console.cloud.google.com/storage/settings](https://console.cloud.google.com/storage/settings)
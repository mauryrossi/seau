import { GoogleCloudStorageDriver } from './drivers/GCS';
import { S3StorageDriver } from './drivers/s3';
import { LocalFileDriver } from './drivers/local';
import { StorageDriver } from './drivers/storage-driver';

const drivers: { [key: string]: any } = {
  s3: S3StorageDriver,
  gcs: GoogleCloudStorageDriver,
  local: LocalFileDriver
};

export class Storage {
  constructor(private disks: { [key: string]: any } = {}) {}
  disk(diskName: string): StorageDriver {
    const driverOptions = this.disks[diskName];
    if (driverOptions === undefined) {
      throw new Error('INVALID_DISK_NAME');
    }
    const driverType: string = driverOptions.driver;
    const Driver = drivers[driverType];
    if (Driver === undefined) {
      throw new Error('INVALID_DISK_NAME');
    }
    return new Driver(driverOptions);
  }
  // returns default disk
  default(): StorageDriver {
    if (this.disks.default == undefined) {
      throw new Error('NO_DEFAULT_DISK');
    }
    return this.disk('default');
  }
}

import '../../test-util';
import { LocalFileDriver } from './local';
import moment = require('moment');
import { join as pathJoin } from 'path';

const client = new LocalFileDriver({
  root: pathJoin(__dirname, '../../testing'),
  url: 'https://site.com/assets'
});

async function cleanupFilesAndDirectories() {
  try {
    await client.deleteDirectory('fs-test-files');
  } catch (err) {}
  try {
    await client.deleteDirectory('fs-test-files-additional');
  } catch (err) {}
}

describe('local.ts', () => {
  beforeAll(async () => {
    await cleanupFilesAndDirectories();
  });
  afterAll(async () => {
    await cleanupFilesAndDirectories();
  });
  describe('#assertExists', () => {
    it('should assert if file exists', async () => {
      let error;
      try {
        await client.put('fs-test-files/assert-exists.txt', 'asdf');
        await client.assertExists('fs-test-files/assert-exists.txt');
      } catch (err) {
        error = err;
      }
      expect(error).toBeUndefined();
    });
  });
  describe('#assertMissing', () => {
    it('should assert if file is missing', async () => {
      let error;
      try {
        await client.assertMissing('fs-test-files/missing-file.txt');
      } catch (err) {
        error = err;
      }
      expect(error).toBeUndefined();
    });
  });
  describe('#exists', () => {
    beforeAll(async () => {
      await client.put('fs-test-files/exists.txt', 'asdf');
    });
    afterAll(async () => {
      await client.delete('fs-test-files/exists.txt');
    });
    it('should return true if file exists', async () => {
      const exists = await client.exists('fs-test-files/exists.txt');
      expect(exists).toBe(true);
    });
    it("should return true if file doesn't exist", async () => {
      const exists = await client.exists('fs-test-files/does-not-exist.txt');
      expect(exists).toBe(false);
    });
  });
  describe('#get/#put', () => {
    beforeAll(async () => {
      await client.put('fs-test-files/get-utf-8.txt', 'asdf');
      await client.put('fs-test-files/get-binary.pdf', Buffer.from('asdf'));
    });
    afterAll(async () => {
      await client.delete('fs-test-files/get-utf-8.txt');
      await client.delete('fs-test-files/get-binary.pdf');
    });
    it('should return string for utf-8 file', async () => {
      const data = await client.get('fs-test-files/get-utf-8.txt', 'utf-8');
      expect(typeof data).toEqual('string');
      expect(data).toEqual('asdf');
    });
    it('should return Buffer if binary', async () => {
      const data = await client.get('fs-test-files/get-binary.pdf', 'binary');
      expect(data instanceof Buffer).toBe(true);
      expect(data).toEqual(Buffer.from('asdf'));
    });
  });
  describe('#delete', () => {
    beforeAll(async () => {
      await client.put('fs-test-files/delete.txt', 'asdf');
    });
    it('should delete a file', async () => {
      await client.delete('fs-test-files/delete.txt');
      const exists = await client.exists('fs-test-files/delete.txt');
      expect(exists).toBe(false);
    });
  });
  describe('#copy', () => {
    beforeAll(async () => {
      await client.put('fs-test-files/copy-me.txt', 'asdf');
    });
    afterAll(async () => {
      await client.delete('fs-test-files/copy-me.txt');
      await client.delete('fs-test-files/copy-me-here.txt');
    });
    it('should copy a file', async () => {
      await client.copy(
        'fs-test-files/copy-me.txt',
        'fs-test-files/copy-me-here.txt'
      );
      const contents = await client.get('fs-test-files/copy-me.txt', 'utf-8');
      expect(contents).toEqual('asdf');
    });
  });
  describe('#move', () => {
    beforeAll(async () => {
      await client.put('fs-test-files/move-me.txt', 'asdf');
      await client.move(
        'fs-test-files/move-me.txt',
        'fs-test-files/move-me-here.txt'
      );
    });
    it('should move a file', async () => {
      expect(await client.exists('fs-test-files/move-me.txt')).toBe(false);
      expect(await client.exists('fs-test-files/move-me-here.txt')).toBe(true);
    });
  });
  describe('#size', () => {
    beforeAll(async () => {
      await client.put('fs-test-files/size.txt', 'asdf');
    });
    afterAll(async () => {
      await client.delete('fs-test-files/size.txt');
    });
    it('should size a file', async () => {
      expect(await client.size('fs-test-files/size.txt')).toEqual(
        Buffer.byteLength('asdf')
      );
    });
  });
  describe('#lastModified', () => {
    beforeAll(async () => {
      await client.put('fs-test-files/last-modified.txt', 'asdf');
    });
    afterAll(async () => {
      await client.delete('fs-test-files/last-modified.txt');
    });
    it('should return a valid date', async () => {
      const modified = await client.lastModified(
        'fs-test-files/last-modified.txt'
      );
      expect(moment(modified).isValid()).toBe(true);
    });
  });
  describe('#url', () => {
    it('should return a valid url', () => {
      const url = client.url('fs-test-files/my-path.txt');
      expect(url).toContain('https://site.com');
    });
  });
  describe('#files/#allFiles/#directories/#allDirectories', () => {
    beforeEach(async () => {
      await cleanupFilesAndDirectories();
      await client.put('fs-test-files-additional/file.txt', 'asdf');
      await client.put('fs-test-files-additional/nested/file.txt', 'asdf');
      await client.put(
        'fs-test-files-additional/nested/another/file.txt',
        'asdf'
      );
      await client.makeDirectory(
        'fs-test-files-additional/nested/yet-another-folder'
      );
    });
    afterEach(async () => {
      await cleanupFilesAndDirectories();
    });
    it('#files should list files only, and not recursively', async () => {
      const files = await client.files('fs-test-files-additional');
      expect(files).toHaveLength(1);
      expect(files[0]).toContain('fs-test-files-additional/file.txt');
    });
    it('#allFiles should list files recursively', async () => {
      const files = await client.allFiles('fs-test-files-additional');
      expect(files).toHaveLength(3);
    });
    it('#directories should list directories only, and not recursively.', async () => {
      const directories = await client.directories('fs-test-files-additional');
      expect(directories).toHaveLength(1);
      expect(directories[0]).toContain('fs-test-files-additional/nested');
    });
    it('#allDirectories should list directories recursively.', async () => {
      const directories = await client.allDirectories(
        'fs-test-files-additional'
      );
      expect(directories).toHaveLength(3);
    });
    it('#directories should list empty subdirectories.', async () => {
      await client.makeDirectory('fs-test-files-additional/last-folder');
      const directories = await client.directories('fs-test-files-additional');
      expect(directories[0]).toContain('fs-test-files-additional/last-folder');
    });
    it('#deleteDirectory should delete directory', async () => {
      await client.makeDirectory('fs-test-files-additional/delete-me-folder');
      await client.deleteDirectory('fs-test-files-additional/delete-me-folder');
      const directories = await client.directories('fs-test-files-additional');
      expect(
        directories.indexOf('fs-test-files-additional/delete-me-folder/')
      ).toEqual(-1);
    });
  });
});

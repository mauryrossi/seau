import moment = require('moment');
import { FileEncoding, FileVisibility, StorageDriver } from './storage-driver';
import { dirname } from 'path';
import { join as pathJoin, resolve as pathResolve } from 'path';
import { pathWithEndingSlash } from '../util';
import { promisify } from 'util';
import {
  readdir as readdirCb,
  readFile as readFileCb,
  writeFile as writeFileCb,
  rename as renameCb,
  unlink as unlinkCb,
  copyFile as copyFileCb,
  stat as statCb,
  mkdir as mkdirCb,
  rmdir as rmdirCb
} from 'fs';

const readdir = promisify(readdirCb);
const readFile = promisify(readFileCb);
const writeFile = promisify(writeFileCb);
const rename = promisify(renameCb);
const unlink = promisify(unlinkCb);
const copyFile = promisify(copyFileCb);
const stat = promisify(statCb);
const mkdir = promisify(mkdirCb);
const rmdir = promisify(rmdirCb);

export interface LocalStorageOptions {
  root?: string;
  url?: string;
}

export class LocalFileDriver extends StorageDriver {
  constructor(private options: LocalStorageOptions) {
    super();
  }

  private pathWithRoot(path: string): string {
    const parsedPath = pathJoin(this.options.root, path);
    return parsedPath;
  }

  public async exists(path: string): Promise<boolean> {
    const fullPath = this.pathWithRoot(path);
    try {
      await stat(fullPath);
      return true;
    } catch (err) {
      return false;
    }
  }

  public async get(
    path: string,
    encoding: FileEncoding = 'utf-8'
  ): Promise<string | Buffer> {
    const fullPath = this.pathWithRoot(path);
    const data = await readFile(fullPath);
    if (encoding === 'binary') {
      return data;
    } else {
      return data.toString(encoding);
    }
  }

  public async put(path: string, contents: string | Buffer): Promise<void> {
    const folderPath = dirname(path);
    await this.makeDirectory(folderPath);

    const fullPath = this.pathWithRoot(path);
    await writeFile(fullPath, contents);
  }

  public async getVisibility(path: string): Promise<FileVisibility> {
    throw new Error('METHOD_NOT_SUPPORTED_BY_DRIVER');
  }

  public async setVisibility(
    path: string,
    visibility: FileVisibility
  ): Promise<void> {
    throw new Error('METHOD_NOT_SUPPORTED_BY_DRIVER');
  }

  public async delete(pathOrPaths: string | string[]): Promise<void> {
    if (Array.isArray(pathOrPaths)) {
      const objects: { Key: string }[] = [];
      for (const path of pathOrPaths) {
        const fullPath = this.pathWithRoot(path);
        await unlink(fullPath);
      }
    } else {
      const fullPath = this.pathWithRoot(pathOrPaths as string);
      await unlink(fullPath);
    }
  }

  public async copy(fromPath: string, toPath: string): Promise<void> {
    const fullFromPath = this.pathWithRoot(fromPath);
    const fullToPath = this.pathWithRoot(toPath);
    await copyFile(fullFromPath, fullToPath);
  }

  public async move(fromPath: string, toPath: string): Promise<void> {
    const fullFromPath = this.pathWithRoot(fromPath);
    const fullToPath = this.pathWithRoot(toPath);
    await rename(fullFromPath, fullToPath);
  }

  public async size(path: string): Promise<number> {
    const fullPath = this.pathWithRoot(path);
    const stats = await stat(fullPath);
    return stats.size;
  }

  public async lastModified(path: string): Promise<string> {
    const fullPath = this.pathWithRoot(path);
    const stats = await stat(fullPath);
    return moment(stats.mtime).format();
  }

  public url(path: string): string {
    return this.options.url + '/' + path;
  }

  public temporaryUrl(path: string, expiresInSeconds: number): string {
    throw new Error('METHOD_NOT_SUPPORTED_BY_DRIVER');
  }

  private async getFiles(
    directory: string,
    files: string[] = [],
    recursive: boolean = false
  ): Promise<string[]> {
    let allFiles = files;
    let directoryListing = await readdir(directory);
    for (const file of directoryListing) {
      const fullPath = pathJoin(directory, file);
      const stats = await stat(fullPath);
      if (stats.isDirectory() && recursive) {
        allFiles = await this.getFiles(fullPath, allFiles, true);
      }
      if (!stats.isDirectory()) {
        allFiles.push(fullPath);
      }
    }
    return allFiles;
  }

  public async files(directory: string = null): Promise<string[]> {
    const fullPath = this.pathWithRoot(directory);
    return this.getFiles(fullPath);
  }

  public async allFiles(directory: string = null): Promise<string[]> {
    const fullPath = this.pathWithRoot(directory);
    return this.getFiles(fullPath, [], true);
  }

  public async directories(directory: string = null): Promise<string[]> {
    const fullPath = this.pathWithRoot(directory);
    return await this.getDirectories(fullPath);
  }

  public async allDirectories(directory: string = null): Promise<string[]> {
    const fullPath = this.pathWithRoot(directory);
    return await this.getDirectories(fullPath, [], true);
  }

  // requires Node 11 for recursive support
  public async makeDirectory(directory: string): Promise<void> {
    if (await this.exists(directory)) {
      return;
    }
    const fullPath = this.pathWithRoot(directory);
    await mkdir(fullPath, {
      recursive: true
    });
  }

  public async deleteDirectory(directory: string): Promise<void> {
    const fullPath = this.pathWithRoot(directory);
    await this.deleteDirectoryAndFiles(fullPath);
  }

  // directory *must* include root path
  private async getDirectories(
    directory: string,
    directories: string[] = [],
    recursive: boolean = false
  ): Promise<string[]> {
    let allDirectories = directories;
    let directoryListing = await readdir(directory);
    for (const file of directoryListing) {
      let fullPath = pathJoin(directory, file);
      const stats = await stat(fullPath);
      if (stats.isDirectory()) {
        fullPath = pathWithEndingSlash(fullPath);
        allDirectories.push(fullPath);
      }
      if (stats.isDirectory() && recursive) {
        allDirectories = await this.getDirectories(
          fullPath,
          allDirectories,
          true
        );
      }
    }
    return allDirectories;
  }

  // directory path *must* include root path
  private async deleteDirectoryAndFiles(directory: string): Promise<void> {
    let directoryListing = await readdir(directory);
    for (const item of directoryListing) {
      const fullPath = pathJoin(directory, item);
      const stats = await stat(fullPath);
      if (stats.isDirectory()) {
        await this.deleteDirectoryAndFiles(fullPath);
      } else {
        await unlink(fullPath);
      }
    }
    await rmdir(directory);
  }
}

export enum FileVisibility {
  PUBLIC = 'PUBLIC',
  PRIVATE = 'PRIVATE'
}

export type FileEncoding = 'utf-8' | 'ascii' | 'binary';

export abstract class StorageDriver {
  public async assertExists(path: string): Promise<void> {
    if (!(await this.exists(path))) {
      throw new Error('FILE_DOES_NOT_EXIST');
    }
  }

  public async assertMissing(path: string): Promise<void> {
    if (await this.exists(path)) {
      throw new Error('FILE_DOES_EXIST');
    }
  }

  abstract async exists(path: string): Promise<boolean>;
  // Get the contents of a file.
  abstract async get(
    path: string,
    encoding: FileEncoding
  ): Promise<string | Buffer>;
  // Store file contents.
  abstract async put(path: string, contents: string | Buffer): Promise<void>;
  // Get the visibility for the given path.
  abstract async getVisibility(path: string): Promise<FileVisibility>;
  // Set the visibility for the given path.
  abstract async setVisibility(
    path: string,
    visibility: FileVisibility
  ): Promise<void>;
  // Delete the file at a given path.
  abstract async delete(paths: string | string[]): Promise<void>;
  // Copy a file to a new location.
  abstract async copy(fromPath: string, toPath: string): Promise<void>;
  // Move a file to a new location.
  abstract async move(fromPath: string, toPath: string): Promise<void>;
  // Get the file size of a given file.
  abstract async size(path: string): Promise<number>;
  // Get the file's last modification time.
  abstract async lastModified(path: string): Promise<string>;
  // Get the URL for the file at the given path.
  abstract url(path: string): string;
  // Get a temporary URL for the file at the given path
  abstract temporaryUrl(path: string, expiresInSeconds: number): string;
  // Get all files in directory (not recursive)
  abstract async files(directory: string | null): Promise<string[]>;
  // Get all of the files from the given directory (recursive).
  abstract async allFiles(directory: string | null): Promise<string[]>;
  // Get all of the directories within a given directory.
  abstract async directories(directory: string | null): Promise<string[]>;
  // Get all (recursive) of the directories within a given directory.
  abstract async allDirectories(directory: string | null): Promise<string[]>;
  // Create a directory.
  abstract async makeDirectory(path: string): Promise<void>;
  // Delete a directory.
  abstract async deleteDirectory(path: string): Promise<void>;
}

import { S3StorageDriver, AwsStorageOptions } from './s3';

export class GoogleCloudStorageDriver extends S3StorageDriver {
  constructor(options: AwsStorageOptions) {
    const parsedOptions: AwsStorageOptions = {
      ...options,
      endpoint: 'https://storage.googleapis.com'
    };
    super(parsedOptions);
  }
}

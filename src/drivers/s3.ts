import S3 = require('aws-sdk/clients/s3');
import moment = require('moment');
import { FileEncoding, FileVisibility, StorageDriver } from './storage-driver';
import { guessMimeType, pathWithEndingSlash } from '../util';
import { dirname } from 'path';

export interface AwsStorageOptions {
  region?: string;
  endpoint?: string;
  bucket: string;
  credentials?: {
    accessKeyId: string;
    secretAccessKey: string;
  };
}

export class S3StorageDriver extends StorageDriver {
  private s3Client: S3;
  private s3Bucket: string;
  constructor(private options: AwsStorageOptions) {
    super();
    this.s3Bucket = options.bucket;
    this.s3Client = new S3(options);
  }

  public async exists(path: string): Promise<boolean> {
    try {
      await this.s3Client
        .headObject({
          Bucket: this.s3Bucket,
          Key: path
        })
        .promise();
      // No errors were thrown, file exists
      return true;
    } catch (err) {
      if (err && err.code == 'NotFound') {
        return false;
      }
      throw err;
    }
  }

  public async get(
    path: string,
    encoding: FileEncoding = 'utf-8'
  ): Promise<string | Buffer> {
    const response = await this.s3Client
      .getObject({
        Bucket: this.s3Bucket,
        Key: path
      })
      .promise();
    if (encoding === 'binary') {
      return response.Body as Buffer;
    } else {
      return response.Body.toString(encoding);
    }
  }

  // Store file contents.
  // TODO: make mime type overrideable

  public async put(path: string, contents: string | Buffer): Promise<void> {
    const mimeType = guessMimeType(path, contents);
    await this.s3Client
      .putObject({
        Bucket: this.s3Bucket,
        Key: path,
        Body: contents,
        ContentType: mimeType
      })
      .promise();
  }

  // Get the visibility for the given path.
  public async getVisibility(path: string): Promise<FileVisibility> {
    const objectAcl = await this.s3Client
      .getObjectAcl({
        Bucket: this.s3Bucket,
        Key: path
      })
      .promise();
    let visibility: FileVisibility = FileVisibility.PRIVATE;
    for (const grant of objectAcl.Grants) {
      if (
        grant.Permission === 'READ' &&
        grant.Grantee.URI &&
        grant.Grantee.URI.indexOf('global/AllUsers') > -1
      ) {
        visibility = FileVisibility.PUBLIC;
      }
    }
    return visibility;
  }

  // Set the visibility for the given path.
  public async setVisibility(
    path: string,
    visibility: FileVisibility
  ): Promise<void> {
    const acl =
      visibility === FileVisibility.PUBLIC ? 'public-read' : 'private';
    await this.s3Client
      .putObjectAcl({
        Bucket: this.s3Bucket,
        Key: path,
        ACL: acl
      })
      .promise();
  }

  // Delete the file at a given path.
  public async delete(paths: string | string[]): Promise<void> {
    if (Array.isArray(paths)) {
      const objects: { Key: string }[] = [];
      for (const path of paths) {
        objects.push({
          Key: path
        });
      }
      await this.s3Client
        .deleteObjects({
          Bucket: this.s3Bucket,
          Delete: {
            Objects: objects
          }
        })
        .promise();
    } else {
      await this.s3Client
        .deleteObject({
          Bucket: this.s3Bucket,
          Key: paths as string
        })
        .promise();
    }
  }

  private async getHeadObject(path: string): Promise<S3.HeadObjectOutput> {
    const head = await this.s3Client
      .headObject({
        Bucket: this.s3Bucket,
        Key: path
      })
      .promise();
    return head;
  }

  // Copy a file to a new location.
  // TODO: consider adding support to copy between buckets

  public async copy(fromPath: string, toPath: string): Promise<void> {
    await this.s3Client
      .copyObject({
        Bucket: this.s3Bucket,
        CopySource: this.s3Bucket + '/' + fromPath,
        Key: toPath
      })
      .promise();
  }

  // Move a file to a new location.
  public async move(fromPath: string, toPath: string): Promise<void> {
    // Only way to move a file in AWS
    await this.copy(fromPath, toPath);
    await this.delete(fromPath);
  }

  // Get the file size of a given file.
  public async size(path: string): Promise<number> {
    const head = await this.s3Client
      .headObject({
        Bucket: this.s3Bucket,
        Key: path
      })
      .promise();
    return head.ContentLength;
  }

  // Get the mime-type of a given file.
  public async mimeType(path: string): Promise<string> {
    const head = await this.getHeadObject(path);
    return head.ContentType;
  }

  // Get the file's last modification time.
  public async lastModified(path: string): Promise<string> {
    const head = await this.getHeadObject(path);
    // Keeping LastModified value consistent with other drivers
    return moment(head.LastModified).format();
  }

  // Get the URL for the file at the given path.
  public url(path: string): string {
    const bucket = this.s3Bucket;
    return `https://${bucket}.s3.amazonaws.com/${path}`;
  }

  // Get a temporary URL for the file at the given path
  public temporaryUrl(path: string, expiresInSeconds: number): string {
    const signedUrl = this.s3Client.getSignedUrl('getObject', {
      Bucket: this.s3Bucket,
      Key: path,
      Expires: expiresInSeconds
    });
    return signedUrl;
  }

  private async getS3Objects(
    directory: string = null,
    delimiter: string = null,
    objects: S3.Object[] = [],
    nextMarker: string = null
  ) {
    const options: S3.ListObjectsRequest = {
      Bucket: this.s3Bucket
    };
    if (directory !== null) {
      let prefix = directory;
      if (prefix[prefix.length - 1] !== '/') {
        prefix += '/';
      }
      options.Prefix = prefix;
    }
    if (delimiter !== null) {
      options.Delimiter = delimiter;
    }
    if (nextMarker !== null) {
      options.Marker = nextMarker;
    }
    const s3Response = await this.s3Client.listObjects(options).promise();
    const s3Objects = s3Response.Contents;
    let combinedObjects = objects;
    for (const object of s3Objects) {
      combinedObjects.push(object);
    }
    for (const prefix of s3Response.CommonPrefixes) {
      combinedObjects.push({
        Key: prefix.Prefix
      });
    }
    if (s3Response.IsTruncated) {
      combinedObjects = await this.getS3Objects(
        directory,
        delimiter,
        combinedObjects,
        s3Response.NextMarker
      );
    }
    return combinedObjects;
  }

  private isDirectory(path: string): boolean {
    if (path && path[path.length - 1] === '/') {
      return true;
    }
    return false;
  }

  // Get all files in directory (not recursive)
  public async files(directory: string = null): Promise<string[]> {
    // Passing / as Delimiter to prevent recursion
    const s3Objects = await this.getS3Objects(directory, '/');
    const files: string[] = [];
    for (const object of s3Objects) {
      if (!this.isDirectory(object.Key)) {
        files.push(object.Key);
      }
    }
    return files;
  }

  // Get all of the files from the given directory (recursive).
  public async allFiles(directory: string = null): Promise<string[]> {
    const s3Objects = await this.getS3Objects(directory);
    const files: string[] = [];
    for (const object of s3Objects) {
      if (!this.isDirectory(object.Key)) {
        files.push(object.Key);
      }
    }
    return files;
  }

  private async getDirectories(
    directoryName: string,
    delimiter: string = null
  ): Promise<string[]> {
    const parsedDirectoryName = pathWithEndingSlash(directoryName);
    const s3Objects = await this.getS3Objects(parsedDirectoryName, delimiter);
    const foldersMap: { [key: string]: boolean } = {};
    const folders: string[] = [];
    for (const object of s3Objects) {
      const folder = this.isDirectory(object.Key)
        ? object.Key
        : dirname(object.Key) + '/';
      if (folder !== parsedDirectoryName && !foldersMap[folder]) {
        foldersMap[folder] = true;
        folders.push(folder);
      }
    }
    return folders;
  }

  // Get all of the directories within a given directory.
  public async directories(directory: string = null): Promise<string[]> {
    return await this.getDirectories(directory, '/');
  }

  // Get all (recursive) of the directories within a given directory.
  public async allDirectories(directory: string = null): Promise<string[]> {
    return await this.getDirectories(directory);
  }

  // Create a directory.
  public async makeDirectory(path: string): Promise<void> {
    const directoryPath = pathWithEndingSlash(path);
    await this.s3Client
      .putObject({
        Bucket: this.s3Bucket,
        Key: directoryPath
      })
      .promise();
  }

  // Delete a directory.
  public async deleteDirectory(path: string): Promise<void> {
    const directoryPath = pathWithEndingSlash(path);
    await this.s3Client
      .deleteObject({
        Bucket: this.s3Bucket,
        Key: directoryPath
      })
      .promise();
  }
}

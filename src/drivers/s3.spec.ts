import '../../test-util';
import { S3StorageDriver } from './s3';
import { back as nockBack, NockBackContext } from 'nock';
import { FileVisibility } from './storage-driver';
import moment = require('moment');

const bucket = 'test-bucket-mauricio';
const region = 'us-east-2';

describe('s3.ts', () => {
  const client = new S3StorageDriver({
    bucket,
    region
  });
  let nockFixture: {
    nockDone: () => void;
    context: NockBackContext;
  };
  describe('#assertExists', () => {
    beforeAll(async () => {
      nockFixture = await nockBack('s3/assert-exists.json');
    });
    afterAll(async () => {
      await nockFixture.nockDone();
    });
    it('should assert if file exists', async () => {
      let error;
      try {
        await client.put('assert-exists.txt', 'asdf');
        await client.assertExists('assert-exists.txt');
      } catch (err) {
        error = err;
      }
      expect(error).toBeUndefined();
    });
  });
  describe('#assertMissing', () => {
    beforeAll(async () => {
      nockFixture = await nockBack('s3/assert-missing.json');
      // ensuring file doesn't exist in S3
      await client.delete('test.txt');
    });
    afterAll(async () => {
      await nockFixture.nockDone();
    });
    it('should assert if file is missing', async () => {
      let error;
      try {
        await client.assertMissing('test.txt');
      } catch (err) {
        error = err;
      }
      expect(error).toBeUndefined();
    });
  });
  describe('#exists', () => {
    beforeAll(async () => {
      nockFixture = await nockBack('s3/exists.json');
      await client.put('exists.txt', 'asdf');
    });
    afterAll(async () => {
      await nockFixture.nockDone();
    });
    it('should return true if file exists', async () => {
      const exists = await client.exists('exists.txt');
      expect(exists).toBe(true);
    });
    it("should return true if file doesn't exist", async () => {
      const exists = await client.exists('does-not-exist.txt');
      expect(exists).toBe(false);
    });
  });
  describe('#get/#put', () => {
    beforeAll(async () => {
      nockFixture = await nockBack('s3/get-put.json');
      await client.put('get-utf-8.txt', 'asdf');
      await client.put('get-binary.pdf', Buffer.from('asdf'));
    });
    afterAll(async () => {
      await nockFixture.nockDone();
    });
    it('should return string for utf-8 file', async () => {
      const data = await client.get('get-utf-8.txt', 'utf-8');
      expect(typeof data).toEqual('string');
      expect(data).toEqual('asdf');
    });
    it('should return Buffer if binary', async () => {
      const data = await client.get('get-binary.pdf', 'binary');
      expect(data instanceof Buffer).toBe(true);
      expect(data).toEqual(Buffer.from('asdf'));
    });
  });
  describe('#getVisibility/#setVisibility', () => {
    it('should return public when file is public', async () => {
      const nockFixture = await nockBack('s3/get-put-public.json');
      await client.put('visible-file.txt', 'asdf');
      await client.setVisibility('visible-file.txt', FileVisibility.PUBLIC);
      const visibility = await client.getVisibility('visible-file.txt');
      expect(visibility).toEqual(FileVisibility.PUBLIC);
      await nockFixture.nockDone();
    });
    // Using two Nock fixtures to allow setVisibility to be called twice
    it('should return private when file is private', async () => {
      const nockFixture = await nockBack('s3/get-put-private.json');
      await client.put('hidden-file.txt', 'asdf');
      await client.setVisibility('hidden-file.txt', FileVisibility.PUBLIC);
      await nockFixture.nockDone();

      const nockFixtureTwo = await nockBack('s3/get-put-private-two.json');
      await client.setVisibility('hidden-file.txt', FileVisibility.PRIVATE);
      const visibility = await client.getVisibility('hidden-file.txt');
      expect(visibility).toEqual(FileVisibility.PRIVATE);
      await nockFixtureTwo.nockDone();
    });
  });
  describe('#delete', () => {
    beforeAll(async () => {
      nockFixture = await nockBack('s3/delete.json');
      await client.put('delete.txt', 'asdf');
    });
    afterAll(async () => {
      await nockFixture.nockDone();
    });
    it('should delete a file', async () => {
      await client.delete('delete.txt');
      const exists = await client.exists('delete.txt');
      expect(exists).toBe(false);
    });
  });
  describe('#copy', () => {
    beforeAll(async () => {
      nockFixture = await nockBack('s3/copy.json');
      await client.put('copy-me.txt', 'asdf');
    });
    afterAll(async () => {
      await nockFixture.nockDone();
    });
    it('should copy a file', async () => {
      await client.copy('copy-me.txt', 'copy-me-here.txt');
      const contents = await client.get('copy-me.txt', 'utf-8');
      expect(contents).toEqual('asdf');
    });
  });
  describe('#move', () => {
    beforeAll(async () => {
      nockFixture = await nockBack('s3/move.json');
      await client.put('move-me.txt', 'asdf');
      await client.move('move-me.txt', 'move-me-here.txt');
    });
    afterAll(async () => {
      await nockFixture.nockDone();
    });
    it('should move a file', async () => {
      expect(await client.exists('move-me.txt')).toBe(false);
      expect(await client.exists('move-me-here.txt')).toBe(true);
    });
  });
  describe('#size', () => {
    beforeAll(async () => {
      nockFixture = await nockBack('s3/size.json');
      await client.put('size.txt', 'asdf');
    });
    afterAll(async () => {
      await nockFixture.nockDone();
    });
    it('should size a file', async () => {
      expect(await client.size('size.txt')).toEqual(Buffer.byteLength('asdf'));
    });
  });
  describe('#mimeType', () => {
    beforeAll(async () => {
      nockFixture = await nockBack('s3/mime-type.json');
      await client.put('plain-text.txt', 'asdf');
      await client.put('binary.pdf', Buffer.from([]));
      await client.put('random-binary.random', Buffer.from([]));
    });
    afterAll(async () => {
      await client.delete('plain-text.txt');
      await client.delete('binary.pdf');
      await client.delete('random-binary.random');
      await nockFixture.nockDone();
    });
    it('should return the mime type', async () => {
      expect(await client.mimeType('plain-text.txt')).toEqual('text/plain');
      expect(await client.mimeType('binary.pdf')).toEqual('application/pdf');
      expect(await client.mimeType('random-binary.random')).toEqual(
        'application/octet-stream'
      );
    });
  });
  describe('#lastModified', () => {
    beforeAll(async () => {
      nockFixture = await nockBack('s3/last-modified.json');
      await client.put('last-modified.txt', 'asdf');
    });
    afterAll(async () => {
      await client.delete('last-modified.txt');
      await nockFixture.nockDone();
    });
    it('should return a valid date', async () => {
      const modified = await client.lastModified('last-modified.txt');
      expect(moment(modified).isValid()).toBe(true);
    });
  });
  describe('#url', () => {
    it('should return a valid url', () => {
      const url = client.url('my-path.txt');
      expect(url).toEqual(
        'https://test-bucket-mauricio.s3.amazonaws.com/my-path.txt'
      );
    });
  });
  describe('#temporaryUrl', () => {
    beforeAll(async () => {
      nockFixture = await nockBack('s3/temporary-url.json');
      await client.put('temporary-url.txt', 'asdf');
    });
    afterAll(async () => {
      await client.delete('temporary-url.txt');
      await nockFixture.nockDone();
    });
    it('should return a valid url', async () => {
      const url = await client.temporaryUrl('temporary-url.txt', 60);
      expect(url.indexOf('https://')).toBeGreaterThan(-1);
      expect(url).toContain(bucket);
      expect(url).toContain('temporary-url.txt');
    });
  });

  describe('#files/#allFiles/#directories/#allDirectories', () => {
    beforeAll(async () => {
      nockFixture = await nockBack('s3/files-and-directories.json');
      await client.put('folder-with-files/file.txt', 'asdf');
      await client.put('folder-with-files/nested/file.txt', 'asdf');
      await client.put('folder-with-files/nested/another/file.txt', 'asdf');
      await client.makeDirectory('folder-with-files/nested/yet-another-folder');
    });
    afterAll(async () => {
      await client.delete('folder-with-files/file.txt');
      await client.delete('folder-with-files/nested/file.txt');
      await client.delete('folder-with-files/nested/another/file.txt');
      await client.deleteDirectory(
        'folder-with-files/nested/yet-another-folder'
      );
      await client.deleteDirectory('folder-with-files/nested/delete-me-folder');
      await nockFixture.nockDone();
    });
    it('#files should list files only, and not recursively', async () => {
      const files = await client.files('folder-with-files');
      expect(files).toHaveLength(1);
      expect(files[0]).toEqual('folder-with-files/file.txt');
    });
    it('#allFiles should list files recursively', async () => {
      const files = await client.allFiles('folder-with-files');
      expect(files).toHaveLength(3);
    });
    // TODO: add more coverage as AWS doesn't like to return empty folders
    it('#directories should list directories only, and not recursively.', async () => {
      const directories = await client.directories('folder-with-files');
      expect(directories).toHaveLength(1);
      expect(directories[0]).toEqual('folder-with-files/nested/');
    });
    it('#allDirectories should list directories recursively.', async () => {
      const directories = await client.allDirectories('folder-with-files');
      expect(directories).toHaveLength(3);
    });
    it('#directories should list empty subdirectories.', async () => {
      await client.makeDirectory('folder-with-files/last-folder');
      const directories = await client.directories('folder-with-files');
      expect(directories).toContain('folder-with-files/last-folder/');
      await client.deleteDirectory('folder-with-files/last-folder');
    });
    it('#deleteDirectory should delete directory', async () => {
      await client.makeDirectory('folder-with-files/delete-me-folder');
      await client.deleteDirectory('folder-with-files/delete-me-folder');
      const directories = await client.directories('folder-with-files');
      expect(
        directories.indexOf('folder-with-files/delete-me-folder/')
      ).toEqual(-1);
    });
  });
});

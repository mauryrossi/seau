import { extname as pathExtension } from 'path';
import * as mimeTypes from './data/mime-types.json';
const mimeTypesObj: { [key: string]: string } = mimeTypes;

export function guessMimeType(path: string, data: string | Buffer): string {
  // Checking if extension (without .) matches a list of known mime types:
  const extension = pathExtension(path).substring(1);
  if (mimeTypesObj[extension] !== undefined) {
    return mimeTypesObj[extension] as string;
  }
  if (data instanceof Buffer) {
    return 'application/octet-stream';
  } else {
    return 'text/plain';
  }
}

export function pathWithEndingSlash(path: string): string {
  let parsedPath = path;
  if (parsedPath[parsedPath.length - 1] !== '/') {
    parsedPath += '/';
  }
  return parsedPath;
}

import { join as pathJoin } from 'path';
import { back as nockBack, NockBackMode } from 'nock';
nockBack.fixtures = pathJoin(__dirname, './testing');
nockBack.setMode((process.env.NOCK_BACK_MODE as NockBackMode) || 'record');
